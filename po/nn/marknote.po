# Translation of marknote to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: marknote\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-08 00:43+0000\n"
"PO-Revision-Date: 2023-07-29 12:06+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: contents/ui/AddNotebookDialog.qml:15 contents/ui/main.qml:70
#, kde-format
msgid "New Notebook"
msgstr "Ny notatbok"

#: contents/ui/AddNotebookDialog.qml:53
#, kde-format
msgid "Notebook Name"
msgstr "Namn på notatbok"

#: contents/ui/AddNotebookDialog.qml:72 contents/ui/NotesPage.qml:160
#: contents/ui/NotesPage.qml:346
#, kde-format
msgid "Add"
msgstr "Legg til"

#: contents/ui/EditPage.qml:75
#, kde-format
msgctxt "@action:button"
msgid "Bold"
msgstr "Halvfeit"

#: contents/ui/EditPage.qml:90
#, kde-format
msgctxt "@action:button"
msgid "Italic"
msgstr "Kursiv"

#: contents/ui/EditPage.qml:101
#, kde-format
msgctxt "@action:button"
msgid "Strikethrough"
msgstr "Gjennomstreking"

#: contents/ui/EditPage.qml:108
#, kde-format
msgctxt "@action:button"
msgid "highlight"
msgstr "Utheving"

#: contents/ui/EditPage.qml:119
#, kde-format
msgid "Add list"
msgstr "Legg til liste"

#: contents/ui/EditPage.qml:127
#, kde-format
msgid "Add numbered list"
msgstr "Legg til nummerert liste"

#: contents/ui/EditPage.qml:137
#, kde-format
msgid "Heading %1"
msgstr "Overskrift %1"

#: contents/ui/main.qml:81 contents/ui/main.qml:173
#, kde-format
msgid "Delete Notebook"
msgstr "Slett notatbok"

#: contents/ui/main.qml:138
#, kde-format
msgid "Your Notebooks"
msgstr "Notatboka dine"

#: contents/ui/NotesPage.qml:35
#, kde-format
msgid "Add note"
msgstr "Legg til notat"

#: contents/ui/NotesPage.qml:46
#, kde-format
msgid "New Note (%1)"
msgstr "Nytt notat (%1)"

#: contents/ui/NotesPage.qml:112
#, kde-format
msgid "Exit Search (%1)"
msgstr "Avslutt søk (%1)"

#: contents/ui/NotesPage.qml:112
#, kde-format
msgid "Search Notes (%1)"
msgstr "Søk i nøtat (%1)"

#: contents/ui/NotesPage.qml:146
#, kde-format
msgid "New Note"
msgstr "Nytt notat"

#: contents/ui/NotesPage.qml:151
#, kde-format
msgid "Note Name:"
msgstr "Notatnamn:"

#: contents/ui/NotesPage.qml:176 contents/ui/NotesPage.qml:287
#: contents/ui/NotesPage.qml:315
#, kde-format
msgid "Delete Note"
msgstr "Slett notat"

#: contents/ui/NotesPage.qml:190
#, kde-format
msgid "Are you sure you want to delete the note <b> %1 </b>?"
msgstr "Er du sikker på at du vil sletta notatet <b>%1</b>?"

#: contents/ui/NotesPage.qml:272 contents/ui/NotesPage.qml:304
#, kde-format
msgid "Rename Note"
msgstr "Endra namn på notat"

#: contents/ui/NotesPage.qml:343
#, kde-format
msgid "Add a Note!"
msgstr "Legg til notat!"

#: contents/ui/WelcomePage.qml:26
#, kde-format
msgid "Start by Creating your first Notebook!"
msgstr "Kom i gang ved å oppretta ei notatbok!"

#: contents/ui/WelcomePage.qml:29
#, kde-format
msgid "Add Notebook"
msgstr "Legg til notatbok"

#: main.cpp:53
#, kde-format
msgid "Marknote"
msgstr "Marknote"

#: main.cpp:55
#, kde-format
msgid "Note taking application"
msgstr "Notatverktøy"

#: main.cpp:57
#, kde-format
msgid "© 2023 Mathis Brüchert"
msgstr "© 2023 Jonah Brüchert"

#: main.cpp:58
#, kde-format
msgid "Mathis Brüchert"
msgstr "Jonah Brüchert"

#: main.cpp:58
#, kde-format
msgid "Maintainer"
msgstr "Vedlikehaldar"

#: main.cpp:59
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#: main.cpp:59
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"
